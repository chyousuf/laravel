@include('header')
<div class="container">
    <div class="row">
        <div class="col-3">
            @include('sidebar')


        </div>
        <div class="col-9 pt-5">
            @if($errors->any())
                <h3> {{ implode('', $errors->all(':message')) }}</h3>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <h2>Add USer</h2>
                <form method="post" action="{{route('CreateUser')}}" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="name" value="Name-admin">
                    <input type="hidden" name="role" value="admin">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <select class="form-control" name="role">
                            <option value="edit_access" >Edit access</option>
                            <option value="only_view" >Only View</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

        </div>
    </div>
</div>
@include('footer')
