@include('header');
<div class="container">
    <div class="row">
        <div class="col-3">
            @include('sidebar');


        </div>
        <div class="col-9 pt-5">
            @if($errors->any())
                <h3> {{ implode('', $errors->all(':message')) }}</h3>
            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <h2>Add Cards</h2>
            <form method="post" action="{{route('cards.store')}}" enctype="multipart/form-data" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Card Title</label>
                    <input type="text" name="Title" class="form-control" id="title" aria-describedby="emailHelp">
                    <label for="exampleInputEmail1" class="form-label">List</label>
                    <select name="list-id" class="form-control">
                        @foreach($listdata as $list)
                        <option value="{{$list->id}}" >{{$list->Title}}</option>
                        @endforeach
                    </select>
                    <label for="exampleInputEmail1" class="form-label">Card Description</label>
                    <textarea class="form-control" cols="30" rows="2" name="Description"></textarea>
                    <label for="exampleInputEmail1" class="form-label pt-3">File</label>
                    <input type="file" name="file">
                </div>
                <button type="submit" class="btn btn-primary">Add Card</button>
            </form>

        </div>
    </div>
</div>
@include('footer');

