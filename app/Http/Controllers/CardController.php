<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\Lists;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Display Page
        $lists = Lists::all();
        return view('Cards')->with(['listdata' => $lists]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate
        $request->validate([
            'Title' => 'required|unique:cards',
            'Description' => 'required',
            'file' => 'required',
        ]);
        //Upload File
        $file = $request->file('file');
        $destinationPath = 'storage/uploads';
        $file->move($destinationPath, $file->getClientOriginalName());
        $file_name = $request->file->getClientOriginalName();
        //Add Data
        $card = new Card();
        $card->Title = $request['Title'];
        $card->Description = $request['Description'];
        $card->file = base64_encode($file_name);
        $card->list_id = $request['list-id'];
        $card->In_complete_checkbox = 'No';
        $card->save();
        return redirect::route('cards.index')->with(["message" => "Card Add Successfully!"]);
    }

    public function create()
    {
        //Show data
        $cards = Card::all();
        return view('Showcards')->with(['carddata' => $cards]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $card = Card::find($id);
        $lists = Lists::all();
        return view('editcard')->with(['card' => $card, 'listdata' => $lists]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {

        //Validate
        $request->validate([
            'Title' => 'required',
            'Description' => 'required',
        ]);
        if (isset($request['file'])) {
            $file_name = $request->file->getClientOriginalName();
        } else {
            $card = Card::find($id);
            $file_name = $card->file;
        }
        Card::findOrFail($id)->update([
            'Title' => $request['Title'],
            'Description' => $request['Description'],
            'file' => base64_encode($file_name),
            'list_id' => $request['list-id'],
            'In_complete_checkbox' => $request['In_complete_checkbox'],
        ]);

        return redirect()->route('cards.create')
            ->with('success', 'Card updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $card = Card::find($id);
        $card->delete();
        return redirect()->route('cards.create')
            ->with('success', 'Card Delete successfully');
    }
}
