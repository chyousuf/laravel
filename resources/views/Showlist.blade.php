@include('header')
<div class="container">
    <div class="row">
        <div class="col-3">
            @include('sidebar')


        </div>
        <div class="col-9">
            <h2>Lists</h2>
            <table>
                <tr>
                    <th>Title</th>
                    <th>Progress bar</th>
                </tr>
                @foreach($listdata as $list)
                    <tr>
                        <td>{{$list->Title}}</td>
                        <td><label for="file">Card Complete progress:</label>
                            @foreach($cards as $card)

                                @if($card->list_id == $list->id)
                                    @php
                                        $count = App\Models\Card::where(['list_id' => $list->id])->count();

                                    @endphp

                                    <progress id="file" value="{{$count}}" max="100"> 32%</progress></td>

                        @endif
                        @endforeach
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@include('footer')
