<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\Lists;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Display the Page
        return view('list');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate
        $request->validate([
            'Title'=>'required|unique:lists',
        ]);
        // Insert Data
        $list = new lists();
        $list->Title = $request['Title'];
        $list->save();
        return redirect::route('lists.index')->with(["message"=>"List Add Successfully!"]);

    }

    public function create()
    {
        //Show data
       $lists =  Lists::all();
       $cards = Card::where('In_complete_checkbox', '=','Yes')->get();
       $count = 0;
       return view('Showlist')->with(['listdata'=>$lists,'cards' => $cards,'count'=>$count]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
