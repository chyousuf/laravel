<?php

use \App\Http\Controllers\UserController;
use \App\Http\Controllers\ListController;
use \App\Http\Controllers\CardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// User

Route::get('/', function () {
    return view('Signup');
});
Route::get('/login', function () {
    return view('login');
})->name('login');
Route::post('/signup', [UserController::class, 'Signup'])->name('signup');
Route::post('/signin', [UserController::class, 'login'])->name('signin');
//Auth Middleware
Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    });
    Route::get('/Create-new-user', [UserController::class, 'view_new_user'])->name('Create-new-user');
    Route::post('/new-user', [UserController::class, 'new_user'])->name('CreateUser');


//List
    Route::resource('lists', ListController::class);
//Cards

    Route::resource('cards', CardController::class);
});



