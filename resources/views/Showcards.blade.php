@include('header')
<div class="container">
    <div class="row">
        <div class="col-3 pt-5">
            @include('sidebar')


        </div>

        <div class="col-9">
            @if (Auth::check())
                @php
                    $role = Auth::user()->role;
                @endphp
            @endif;
            <h2>Cards</h2>
            @if($errors->any())
                <h3> {{ implode('', $errors->all(':message')) }}</h3>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <table>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>File/Image</th>
                    @if($role != 'only_view')
                        <th>Edit</th>
                        <th>Delete</th>
                    @endif
                </tr>
                @foreach($carddata as $card)
                    <tr>
                        <td>{{$card->Title}}</td>
                        <td>{{$card->Description}}</td>
                        <td><a href="{{ asset("/storage/uploads/".base64_decode($card->file))}}" download>Download</a>
                        </td>
                        @if($role != 'only_view')
                            <td><a href="{{route( 'cards.edit',$card->id )}}">Edit</td>
                            <td>
                                <form action="{{route('cards.destroy',$card->id )}}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <input type="submit" value="Delete">
                                </form>
                            </td>
                        @endif
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@include('footer')
