<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
   public function Signup(Request  $request){
       $request->validate([
          'email'=>'required|unique:users',
           'password'=> 'required'
       ]);
       $user = new User();
       $user->password = Hash::make($request['password']);
       $user->email = $request['email'];
       $user->name = $request['name'];
       $user->role = $request['role'];
       $user->save();
     return redirect::to('dashboard');
   }
    public function login(Request $request)
    {
        $request->validate([
            'email'=>'required',
            'password'=> 'required'
        ]);
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        } else{
            return redirect::to('login')->withErrors(["message"=>"Email And Password not match"]);
        }
    }
    public function view_new_user(){
       return view('create_user');
    }
   public function new_user(Request  $request){
       $request->validate([
           'email'=>'required|unique:users',
           'password'=> 'required'
       ]);
       $user = new User();
       $user->password = Hash::make($request['password']);
       $user->email = $request['email'];
       $user->name = $request['name'];
       $user->role = $request['role'];
       $user->save();
       return redirect::route('Create-new-user')->withErrors(["success"=>"User Created Successfully"]);
   }
}
