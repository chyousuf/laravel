@include('header')
<div class="container">
    <div class="row">
        <div class="col-3">
            @include('sidebar')
        </div>
        <div class="col-9 pt-5">
            @if($errors->any())
                <h3> {{ implode('', $errors->all(':message')) }}</h3>
            @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
            <h2>Add List</h2>
            <form method="post" action="{{route('lists.store')}}" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">List Title</label>
                    <input type="text" name="Title" class="form-control" id="title" aria-describedby="emailHelp">
                </div>
                <button type="submit" class="btn btn-primary">Add List</button>
            </form>

        </div>
    </div>
</div>
@include('footer')

