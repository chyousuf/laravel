<?php
/*
Template Name: Login
*/  
get_header();
$is_admin = current_user_can('manage_options');
if($is_admin){
	$url = get_permalink( '1714' ); 
	?>
	<script>
		var url = "<?php echo $url;?>";
		jQuery(location).attr('href',url);
	</script>
<?php }else{
	?>
	<style>
	.button-register{
		padding: 10px 10px 10px 10px;
		margin-bottom: 10px;
	}
</style>
<?php
global $reg_errors;
$reg_errors = new WP_Error;
$phone = $_POST['phone'];
$password = $_POST['password'];
if (isset($_POST['submit']) && isset($phone) && isset($password) ){
	$login_password = wp_hash_password($_POST['password']);
        $user = get_users(
                      array(
                       'meta_key' => 'phone',
                       'meta_value' => $phone,
                       'compare'=>'LIKE',
                      )
                    );

            // Get the hashed password from the post.
        $hashed_password = $user[0]->data->user_pass; 
         $user_id = $user[0]->data->ID; 
         $pass = get_field("code","user_".$user_id);
   var_dump($user);
        // Compare the hashed passwords.
        if ($pass == $password) { 
            wp_redirect(get_the_permalink( '2969' )); 
            }else{
            	
    $reg_errors->add( 'Not Match', 'Phone or Password not math' );

            }
    
}
?> 
<style>
.header-style-11 .header-upper .inner-container{
	background: #242c36 !important;}
	.contact-form .bootstrap-select>.dropdown-toggle, .contact-form .form-group input[type="tel"], .contact-form .form-group input[type="password"], .contact-form .form-group textarea, .contact-form .form-group select {
		position: relative;
		display: block;
		height: 54px;
		width: 100%;
		font-size: 15px;
		color: #738299;
		line-height: 30px;
		font-weight: 600;
		background-color: transparent;
		border-radius: 6px;
		padding: 11px 20px;
		border-width: 1px;
		border-style: solid;
		border-color: rgb(225, 229, 233);
		-o-border-image: initial;
		border-image: initial;
		-webkit-transition: all 300ms ease 0s;
		-o-transition: all 300ms ease 0s;
		transition: all 300ms ease 0s;
	}
</style>
<?php  echo $error; 
echo $user;
 ?>
<div id="main" style="margin-top: 300px;">
	<section class="block">
		<div class="container contact-form-box">
			<h2 class="text-center mb-5">Login</h2>
			<?php
			if ( is_wp_error( $reg_errors ) ) {
				foreach ( $reg_errors->get_error_messages() as $error ) {
					echo '<div style="color:red;">';
					echo '<strong>ERROR</strong>:';
					echo $error . '<br/>';
					echo '</div>';
				}
			}
			?>
			<form id="login" class="w-100" action="#" method="post" >
				<div class="row contact-form">
					<div class="col-md-6 form-group" >
						<label for="email">Phone <strong>*</strong></label>
						<input class="form-control" type="tel" placeholder="0000-0000000" name="phone"   required="required">
					</div>
					<div class="col-md-6 form-group">
						<label for="password">Password <strong>*</strong></label>
						<input class="form-control" type="password" name="password"  required="required">
					</div>
					<br>
					<div class="col-md-12 text-center  form-group">
						<input class="theme-btn btn-style-four button-register" type="submit" name="submit" value="Login"/>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
</div>
<?php } ?>
<script>
	jQuery(document).ready(function () {  
		jQuery("#wp-submit").click(function(){
			var user_login = jQuery('#user_login').val();
			var user_pass = jQuery('#user_pass').val();
			if (user_login == '' || user_login == undefined) {
				jQuery('#user_login').css({
					"border": "1px solid red"
				});
				return false;
			}else{
				jQuery('#user_login').css({
					"border": ""
				});
			}
			if (user_pass == '' || user_pass == undefined) {
				jQuery('#user_pass').css({
					"border": "1px solid red"
				});
				return false;
			}else{
				jQuery('#user_pass').css({
					"border": ""
				});
			}
		});
	});
	var Submit = jQuery("#wp-submit");
	jQuery(Submit).on("click", function (e) {
		var v = grecaptcha.getResponse();
		if(v.length == 0)
		{
			document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
			return false;
		}
		else
		{
			jQuery('#recptcha').val('Done');
		}
	});
</script>
<?php  get_footer();  ?>