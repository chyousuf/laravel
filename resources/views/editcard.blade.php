@include('header');
<div class="container">
    <div class="row">
        <div class="col-3">
            @include('sidebar');


        </div>
        <div class="col-9 pt-5">
            @if($errors->any())
                <h3> {{ implode('', $errors->all(':message')) }}</h3>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <h2>Update Cards</h2>
            <form method="post" action="{{route('cards.update',$card->id)}}" enctype="multipart/form-data" >
                {{ method_field('PUT') }}
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Card Title</label>
                    <input type="text" name="Title" class="form-control" value="{{$card->Title}}" id="title" aria-describedby="emailHelp">
                    <label for="exampleInputEmail1" class="form-label">List</label>
                    <select name="list-id" class="form-control">
                        @foreach($listdata as $list)
                            <option value="{{$list->id}}" @if($list->id==$card->list_id){{'selected'}}@endif>{{$list->Title}}</option>
                        @endforeach
                    </select>
                    <label for="exampleInputEmail1" class="form-label">Card Description</label>
                    <textarea class="form-control" cols="30" rows="2" name="Description">{{$card->Description}}</textarea>
                    <label for="exampleInputEmail1" class="form-label pt-3">File</label>
                    <input type="file" name="file" value="{{base64_decode($card->file)}}"><a href="{{ asset("/storage/uploads/".base64_decode($card->file))}}" download>Old File Download</a><br>
                    <label for="exampleInputEmail1" class="form-label pt-3">Task Card</label><br>
                    <input type="checkbox"  name="In_complete_checkbox" value="Yes"  @if($card->In_complete_checkbox=='Yes'){{'checked'}}@endif>
                    <label for="vehicle1">Complete Task Card</label><br>
                </div>
                <button type="submit" class="btn btn-primary">Update Card</button>
            </form>

        </div>
    </div>
</div>
@include('footer');

