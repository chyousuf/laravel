<div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>Menu Sidebar</h3>
        </div>
        @if (Auth::check())
            @php
                $role = Auth::user()->role;
            @endphp

            @if($role == 'only_view')
                <ul class="list-unstyled components">
                    <li><a href="{{route('lists.create')}}">Show all List</a></li>
                    <li><a href="{{route('cards.create')}}">Show all Cards</a></li>
                </ul>
            @else
                <ul class="list-unstyled components">
                    <li>
                        <a href="{{route('lists.index')}}">Add Lists</a>
                        <ul>
                            <li><a href="{{route('lists.create')}}">Show all List</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('cards.index')}}">Add Cards</a>
                        <ul>
                            <li><a href="{{route('cards.create')}}">Show all Cards</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('Create-new-user')}}">Add User</a>
                    </li>

                </ul>
            @endif
        @endif
    </nav>
</div>
